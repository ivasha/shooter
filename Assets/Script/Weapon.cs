﻿using Assets.Script.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script
{
    public abstract class Weapon : BaseObjectScene
    {
        [SerializeField] protected Ammunition _bullet;

        [SerializeField] protected Sprite _sprite;
        [SerializeField] protected JoinPoint _joinPoint;
        [SerializeField] protected float _force = 0.05f;
        [SerializeField] protected float _rechargeTime = 0.2f;
        [SerializeField] protected Transform _sight;

        [SerializeField] protected string _text;
        [SerializeField] protected KeyCode _keyCode = KeyCode.E;

        protected bool _fire = true;

        protected Timer _recharge = new Timer(); // Выделяем память под таймер

        public abstract void Fire();


        protected virtual void  Update()
        {
            if (!_isEnabled) return;
            _recharge.Update(); // Производим подсчёты времени
            if (_recharge.IsEvent())// Если закончили отсчёт, разрешаем стрелять
            {
                _fire = true;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            //_joinPoint = MyGameObject.transform.Find("JoinPoint");
            _joinPoint = FindObjectOfType<JoinPoint>();
        }

        public void InInventory()
        {
            MyGameObject.transform.position = _joinPoint.Position;
            MyGameObject.transform.rotation = _joinPoint.Rotation;
            transform.SetParent(_joinPoint.transform);
        }

        public void FromInventory()
        {
            transform.parent = null;
        }

        public override void EnableRigidBody()
        {
            base.EnableRigidBody();
            _rigidbody.AddForce(transform.forward * _force);
        }


        public Sprite GetSprite()
        {
            return _sprite;
        }

        public KeyCode GetKeyCode()
        {
            return _keyCode;
        }

        public string GetMessage()
        {
            return _text + " " + _keyCode;
        }
    }
}
