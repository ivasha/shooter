﻿
using System;
using UnityEngine;

namespace Assets.Script
{
    public class Bot : Enemy
    {

        protected readonly float _activeAngle = 70;
        private readonly float _timeWait = 3;
        private TargetSearcher _targetSearcher;
        private PathSearcher _pathSearcher;
        void Start()
        {
            _pathSearcher = GetComponent<PathSearcher>();
            _targetSearcher = new TargetSearcher(transform, Target);

        }

        protected override void Update()
        {
            base.Update();
            _isTarget = _targetSearcher.TargetSearchAngle(_activeDistance, _activeAngle);
            _pathSearcher.PathSearchOnWayPoint(Agent, _isTarget, _stoppingDistance, _timeWait, Target);
        }

        public static explicit operator Bot(GameObject v)
        {
            throw new NotImplementedException();
        }
    }
}

