﻿using UnityEngine;

namespace Assets.Script
{
    public abstract class Ammunition : BaseObjectScene
    {

        [SerializeField] protected float _timeToDestruct = 10; // Время жизни пули
        [SerializeField] protected float _damage = 20; // Урон пули
        [SerializeField] protected float _mass = 0.01f; // Масса пули
        protected float _currentDamage; // Текущий урон, который может нанести пуля

        protected override void Awake()
        {
            base.Awake();
           // Destroy(MyGameObject, _timeToDestruct); 
            _currentDamage = _damage;
            Rigidbody.mass = _mass;


        }
    }
}
