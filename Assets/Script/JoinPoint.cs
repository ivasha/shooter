﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinPoint : MonoBehaviour {

    private Vector3 _position;
    private Quaternion _rotation;

    public Vector3 Position
    {
        get
        {
            _position = transform.position;
            return _position;
        }
    }

    public Quaternion Rotation
    {
        get
        {
            _rotation = transform.rotation;
            return _rotation;
        }
    }
}
