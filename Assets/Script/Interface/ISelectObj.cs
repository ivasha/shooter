﻿using UnityEngine;

namespace Assets.Script.Interface
{
    public interface ISelectObj
    {
        string GetMessage();
        KeyCode GetKeyCode();
        Sprite GetSprite();
    }
}

