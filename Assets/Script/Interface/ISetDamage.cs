﻿namespace Assets.Script.Interface
{
    public interface ISetDamage
    {
        void ApplyDamage(float damage);
    }

}
