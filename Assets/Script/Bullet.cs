﻿
using Assets.Script.Interface;
using UnityEngine;

namespace Assets.Script
{
    public class Bullet : Ammunition
    {

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.tag == "Bullet") return;
            SetDamage(collision.gameObject.GetComponent<ISetDamage>());

            //Destroy(MyGameObject); // Удаляем пулю
        }

        private void SetDamage(ISetDamage obj)
        {
            if (obj != null)
                obj.ApplyDamage(_currentDamage); // Вызываем функцию получения урона
        }

    }
}
