﻿
using System;
using Assets.Script.Interface;
using UnityEngine;
using UnityEngine.AI;
using Assets.Script.Helper;

namespace Assets.Script
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(PathSearcher))]
    [RequireComponent(typeof(Path))]


    public abstract class Enemy : Character
    {
        public Transform Target;
        public int _activeDistance = 50;

        public NavMeshAgent Agent;
        protected float _stoppingDistance = 4f;
        protected bool _isTarget; // Если нашли цель (В данном случае нас)

        protected override void Awake()
        {
            base.Awake();
            _isDead = false;
            Agent = GetComponent<NavMeshAgent>();
            Target = GameObject.FindGameObjectWithTag("Player").transform;

        }

        protected virtual void Update()
        {
            if (_isDead|| !Target) return;
        }

        protected override void DeadCharacter()
        {
            base.DeadCharacter();
            Agent.enabled = false;
            Destroy(MyGameObject, 10);

        }

    }
}
