﻿using Assets.Script.Helper;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathSearcher : MonoBehaviour {

    private readonly int radius = 10;
    public List<Vector3> _wayPoints = new List<Vector3>();
    private float _curTimeout;
    private int _wayCount = 5;
    private int _currentPoint = 0;
    private Path path;


    void Start () {
        GeneratePath();
        path = GetComponent<Path>();
        if (path)
        {
            path.SetPath(_wayPoints);
        }
    }

    private void GeneratePath()
    {
        for (int i = 0; i < _wayCount; i++)
        {
            Vector3 _position = GenerateRandomPoint(radius);
            _wayPoints.Add(_position);
        }
       
    }

    public void PathSearchOnWayPoint(NavMeshAgent agent, bool isTarget, float _stoppingDistance, float timeWait, Transform target)
    {
        if (_wayPoints.Count >= 2 && ! isTarget)
        {            
            agent.stoppingDistance = 0;
            agent.SetDestination(_wayPoints[_currentPoint]);

            if (!agent.hasPath) // Если нам некуда идти
            {
                _curTimeout += Time.deltaTime; // Отсчитываем время
                if (_curTimeout > timeWait)
                {
                    _curTimeout = 0;
                    GenerationWayPoints();
                }
            }
        }
        else if (_wayPoints.Count == 0 || isTarget)
        {
            agent.stoppingDistance = _stoppingDistance;
            agent.SetDestination(target.position);
        }
    }

    private void GenerationWayPoints()
    {
        _currentPoint =  Random.Range(0, _wayPoints.Count);
    }
    
    public void PathSearchRandomInsideSphere(int radius, NavMeshAgent agent)
    {
        Vector3 _position = GenerateRandomPoint(radius);
        agent.SetDestination(_position);
    }

    private Vector3 GenerateRandomPoint(int radius)
    {
        Vector3 randomPos = Random.insideUnitSphere * radius;
        NavMeshHit navMeshHit;
        NavMesh.SamplePosition(transform.position + randomPos, out navMeshHit, radius, NavMesh.AllAreas);
        return navMeshHit.position;
    }
}
