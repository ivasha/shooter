﻿using Assets.Script.Controller;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

namespace Assets.Script
{
    public class MovingPoints : MonoBehaviour
    {
        public AICharacterControl _agent;
        [SerializeField] private GameObject _point;
        private Queue<GameObject> _points;
        private Vector3 _position;
        private readonly Color _c1 = Color.red;
        private readonly Color _c = Color.red;
        private readonly int lengthOfLineRenderer = 2;
        private LineRenderer _lineRenderer;

        private void Start()
        {
            _points = new Queue<GameObject>();
            _lineRenderer = gameObject.AddComponent<LineRenderer>();
            _lineRenderer.SetWidth(0.5F, 0.5F);
            //_lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
            _lineRenderer.SetColors(_c, _c1);
            _lineRenderer.SetVertexCount(lengthOfLineRenderer);
            _lineRenderer.SetPosition(0, _agent.transform.position);
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown((int) MouseButton.LeftButton))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                {
                    DrawPoint(hit.point);
                }
            }

            RaycastHit hitInfo;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
            {
                _position = hitInfo.point;
            }
            _lineRenderer.SetPosition(1, _position);

                if (_agent.target == null && _points.Count > 0)
                {
                    GotoNextPoint();
                }

        }
        private void DrawPoint(Vector3 position)
        {
            GameObject tempPoint = ObjectPooler.Instance.GetPooledGameObject("Point");
            if (tempPoint)
            {
                tempPoint.transform.position = position;
                tempPoint.transform.rotation = Quaternion.identity;
                tempPoint.SetActive(true);
                _points.Enqueue(tempPoint);
                _lineRenderer.SetPosition(0, tempPoint.transform.position);
            }
        }
        public void GotoNextPoint() 
        {
            _agent.SetTarget(_points.Dequeue().transform);
           
        }

        public int  GetCountPoint()
        {
            return _points.Count;
        }
    }
}
