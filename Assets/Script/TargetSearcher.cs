﻿using UnityEngine;

namespace Assets.Script
{
    class TargetSearcher 
    {
        private Transform _enemyTransfom;
        private Transform _target;

        public TargetSearcher(Transform enemy, Transform target) {
            _enemyTransfom = enemy;
            _target = target;
        }

        public bool TargetSearchAngle(int activeDistance, float activeAngle)
        {
            float dis = Vector3.Distance(_enemyTransfom.position, _target.position);

            if (dis < activeDistance)
            {
                if (Vector3.Angle(_enemyTransfom.forward, _target.position - _enemyTransfom.position) <= activeAngle)
                {
                    if (!CheckBlocked()) 
                    {
                        return true; 
                    }
                }
            }
            return false;
        }
            

        private bool CheckBlocked()
        {
            RaycastHit hit;
            Debug.DrawLine(_enemyTransfom.position, _target.position, Color.red);
            if (Physics.Linecast(_enemyTransfom.position, _target.position, out hit))
            {
                if (hit.transform == _target)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
