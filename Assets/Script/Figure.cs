﻿
using UnityEngine;

public class Figure : MonoBehaviour {


    Color[] _colors = new Color[]
    {
            Color.green, Color.black, Color.blue, Color.clear, Color.cyan, Color.red, Color.yellow,
            Color.white,
            Color.red
    };

   private GameObject _prefab;
   private int _countObject;
   private float _radius;
   private string _nameObject;
   private bool _randomColor;

    public Figure(GameObject prefab, int countObject, float radius, string nameObject, bool randomColor)
    {
        this._prefab = prefab;
        this._countObject = countObject;
        this._radius = radius;
        this._nameObject = nameObject;
        this._randomColor = randomColor;
            
    }

    private GameObject root;

    public int MyProperty { get; set; }



}
