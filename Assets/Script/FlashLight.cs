﻿using UnityEngine;

namespace Assets.Script
{
    class FlashLight : BaseObjectScene
    {
        [HideInInspector]
        public float CurrentWorkingHours;
        public float MaxWorkingHours = 30;

        private Light _light;

        protected void Start()
        {
            CurrentWorkingHours = MaxWorkingHours;
        }

        protected override void Awake()
        {
            base.Awake();
            _light = GetComponent<Light>();
            Switch(false);
        }

        public void Switch(bool value)
        {
            if (!_light) return;
            _light.enabled = value;
            
        } 
    }
}
