﻿
using UnityEngine;
using UnityEngine.UI;

public class UIMessage : MonoBehaviour {

    private Text _message;

    void Awake()
    {
        _message = GetComponent<Text>();
        SetText(string.Empty);
    }

    public void SetText(string value)
    {
        if(_message)
        {
            _message.text = value;
        }
    }
}
