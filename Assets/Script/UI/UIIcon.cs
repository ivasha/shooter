﻿
using UnityEngine;
using UnityEngine.UI;

public class UIIcon : MonoBehaviour {

    private Image _icon;
	// Use this for initialization
	void Start () {
        _icon = GetComponent<Image>();
        SetIcon(null, false);
	}
	
    public void SetIcon(Sprite sprite, bool value)
    {
        _icon.enabled = value;
        _icon.sprite = sprite;
    }
}
