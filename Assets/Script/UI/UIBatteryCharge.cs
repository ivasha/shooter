﻿using UnityEngine;
using UnityEngine.UI;

public class UIBatteryCharge : MonoBehaviour {

    private Image _batteryCharge;


    // Use this for initialization
    void Start () {
        _batteryCharge = GetComponent<Image>();
        SetColorBattery(0, false);

    }
	
    public void SetColorBattery(float batteryCharge, bool active)
    {
        _batteryCharge.enabled = active;
       _batteryCharge.material.color = Color.white;  //Color.Lerp(Color.green, Color.red, 1 - batteryCharge * 0.01f);
        // _batteryCharge.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
    }
}
