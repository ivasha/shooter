﻿using Assets.Script.Interface;
using System;
using UnityEngine;

namespace Assets.Script
{
    public class Gun : Weapon, ISelectObj
    {
        public override void Fire()
        {
            if (_fire)
            {

                if (_bullet)
                {

                    Bullet tempbulet = Instantiate(_bullet, _sight.position, _sight.rotation) as Bullet;
                    if (tempbulet)
                    {
                        tempbulet.Rigidbody.useGravity = true;
                        Debug.Log("FIRE!!!");
                        tempbulet.Rigidbody.AddForce(_sight.forward * _force);// Добавляем ускорение к пуле
                        tempbulet.Name = "Bullet"; // Задаём имя пуле
                        _fire = false; // Сообщаем, что произошёл
                      _recharge.Start(_rechargeTime); // Запускаем таймер
                    }
                }
            }
        }

    }
}

