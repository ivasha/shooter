﻿using UnityEngine;

namespace Assets.Script
{
    public class BaseObjectScene : MonoBehaviour
    {

        [HideInInspector]
        public Transform MyTransform;
        [HideInInspector]
        public GameObject MyGameObject;

        protected Collider _collider;
        protected Rigidbody _rigidbody;
        protected bool _isEnabled;


        private Color _color;
        private Material _material;
        private Vector3 _position;
        private Quaternion _rotation;
        private Vector3 _scale;
        private int _layer;
        private string _name;

        public bool IsEnabled { get { return _isEnabled; } }

        public string Name
        {
            get
            {
                _name = MyGameObject.name;
                return _name;
            }
            set
            {
                _name = value;
                MyGameObject.name = _name;
            }
        }

        public int Layer
        {
            get
            {
                _layer = MyGameObject.layer;
                return _layer;
            }
            set
            {
                _layer = value;
                SetLayer(MyTransform, _layer);
            }
        }

        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                if (_material != null)
                {
                    _material.color = value;

                }

                SetColor(MyTransform, _color);
            }
        }

        public Collider Collider
        {
            get { return _collider; }
        }

        //public Vector3 Position
        //{
        //    get
        //    {
        //        if (MyGameObject != null)
        //        {
        //            _position = MyTransform.position;
        //        }
        //        return _position;
        //    }
        //    set
        //    {
        //        _position = value;
        //        if (MyGameObject != null)
        //        {
        //            MyTransform.position = _position;
        //        }
        //    }
        //}

        //public Quaternion Rotation
        //{
        //    get
        //    {
        //        if (MyGameObject != null)
        //        {
        //            _rotation = MyTransform.rotation;
        //        }
        //        return _rotation;
        //    }
        //    set
        //    {
        //        _rotation = value;
        //        if (MyGameObject != null)
        //        {
        //            MyTransform.rotation = _rotation;
        //        }
        //    }
        //}

        public Rigidbody Rigidbody
        {
            get { return _rigidbody; }
        }


        private void SetColor(Transform myTransform, Color color)
        {
            if (myTransform.gameObject.GetComponent<Renderer>())
            {
                myTransform.gameObject.GetComponent<Renderer>().material.color = color;
            }
            if (myTransform.childCount > 0)
            {
                foreach (Transform transform in myTransform)
                {
                    SetColor(transform, color);
                }

            }
        }

        private void SetLayer(Transform myTransform, int layer)
        {
            myTransform.gameObject.layer = layer;
            if (myTransform.childCount > 0)
            {
                foreach (Transform transform in myTransform)
                {
                    SetLayer(transform, layer);
                }

            }
        }

        protected virtual void Awake()
        {
            if (GetComponent<Renderer>())
            {
                _material = GetComponent<Renderer>().material;
                _color = _material.color;

            }

            if (GetComponent<Rigidbody>())
            {
                _rigidbody = GetComponent<Rigidbody>();
            }

            MyGameObject = gameObject;
            MyTransform = transform;

        }


        public void SetActive(bool value)
        {
            Renderer renderGameObject = GetComponent<Renderer>();
            if (renderGameObject)
            {
                renderGameObject.enabled = value;
                if (transform.childCount > 0)
                {
                    foreach (Transform t in transform)
                    {
                        Renderer childRenderer = t.gameObject.GetComponent<Renderer>();
                        if (childRenderer)
                        {
                            childRenderer.enabled = value;
                        }
                    }
                }
                if (_collider)
                {
                    _collider.enabled = value;
                }
                _isEnabled = value;
            }
        }

        public void DisableRigidBody()
        {
            if (_rigidbody)
            {
                _rigidbody.isKinematic = true;
            }
        }

        public virtual void EnableRigidBody()
        {
            if (!_rigidbody)
            {
                _rigidbody = gameObject.AddComponent<Rigidbody>();
            }
            _rigidbody.isKinematic = false;
        }

    }
}


