﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Helper
{
    public sealed class Timer
    {
        DateTime _start;
        float _elapsed = -1;
        private TimeSpan _duration;

        public TimeSpan Duration { get { return _duration; } }

        public void Start(float elapsed)
        {
            _elapsed = elapsed;
            _start = DateTime.Now;
            _duration = TimeSpan.Zero;
        }
        public void Update()
        {
            if (_elapsed > 0)
            {
                _duration = DateTime.Now - _start;
                if (Duration.TotalSeconds > _elapsed)
                {
                    _elapsed = 0;
                }
            }
            else if (_elapsed == 0)
            {
                _elapsed = -1;
            }
        }
        public bool IsEvent()
        {
            return _elapsed == 0;
        }
    }

}

