﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Script.Helper
{
    public class Path : MonoBehaviour
    {
        private List<Vector3> nodes;
        
        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    Vector3 currentNode = nodes[i];
                    Vector3 previousNode = Vector3.zero;
                    if (i > 0)
                    {
                        previousNode = nodes[i - 1];
                    }
                    else if (i == 0 && nodes.Count > 1)
                    {
                        previousNode = nodes[nodes.Count - 1];
                    }
                    Gizmos.DrawLine(previousNode, currentNode);
                    Gizmos.DrawWireSphere(currentNode, 0.3f);
                }
            }

        }

        public void SetPath(List<Vector3> wayPoints)
        {
            nodes = wayPoints;
        }
    }
}

