﻿using Assets.Script.Interface;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script
{
    public class Inventory : MonoBehaviour
    {
        [SerializeField] private int _maxNumberWeapon = 5;

        private List<Weapon> _weapons = new List<Weapon>();
        private int _currentNumberWeapon;

        private Weapon _weapon;
        private UIIcon _icon;
        private UIMessage _uimessage;
        private bool _isActiveWeapon = false;

        public bool IsActiveWeapon { get { return _isActiveWeapon; } private set { _isActiveWeapon = value; } }


        public int CountWeapon { get { return _weapons.Count; } }
        // Use this for initialization
        void Start()
        {
            _icon = FindObjectOfType<UIIcon>();
            _uimessage = FindObjectOfType<UIMessage>();
        }

        public Weapon GetWeaponByIndex(int index)
        {
            return _weapons[index];
        }


        public void AddObjectToInventory(ISelectObj selectObj)
        {
            if (_weapons.Count < _maxNumberWeapon)
            {
                Weapon tempWeapon = selectObj as Weapon;
                if (tempWeapon)
                {
                    _weapon = tempWeapon;
                    _weapon.InInventory();
                    _weapon.SetActive(false);
                    _weapon.DisableRigidBody();
                    _weapons.Add(_weapon);
                    Debug.Log("count" + _weapons.Count);

                }
            } else if (_weapons.Count > _maxNumberWeapon)
            {
                _uimessage.SetText("Инвентарь заполнен!");
            }
        }

        public void DeactivateWeapon(int index)
        {

            _weapon = GetWeaponByIndex(index);

            if (_weapon)
            {
                IsActiveWeapon = false;
                _weapon.SetActive(false);
                _icon.SetIcon(null, false);
            }
        }

        public void ActivateWeapon(int index)
        {
            _weapon = GetWeaponByIndex(index);

            if (_weapon)
            {
                foreach (Weapon weapon in _weapons)
                {
                    weapon.SetActive(false);
                }
                 _weapons[index].SetActive(true);
                _icon.SetIcon(_weapon.GetSprite(), true);

            }

            _isActiveWeapon = true;
        }

        public void Remove()
        {
            if (!_weapon) return;
            _weapon.SetActive(true);
            _weapon.FromInventory();
            _weapon.EnableRigidBody();
            _icon.SetIcon(_weapon.GetSprite(), false);
            _weapons.Remove(_weapon);
            _weapon = null;

        }

    }
}

