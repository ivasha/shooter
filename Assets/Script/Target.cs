﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    private MovingPoints mp;

    private void Awake()
    {
         mp = Camera.main.GetComponent<MovingPoints>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "AIThirdPersonController")
        {
            gameObject.SetActive(false);
            mp._agent.target = null;
        }
    }
}
