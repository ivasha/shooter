﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Script.Controller
{
    public class WeaponController : BaseController
    {
        private Weapon _weapon;
        private int _mouseButton = (int) MouseButton.LeftButton;

        public Weapon SelectedWeapon
        {
            get { return _weapon; }
        }

        public void Update()
        { 
            if (!Enabled) return;
            if (Input.GetMouseButton(_mouseButton) && _weapon.IsEnabled) 
            {
                SelectedWeapon.Fire();
            }
        }

        public virtual void On(Weapon weapon)
        {
            if (Enabled) return;
            base.On();

            _weapon = weapon;

            //_weapon.IsVisible = true;
        }
        public override void Off()
        {
            if (Enabled) return;
            base.Off();
            _weapon = null;
           // _weapon.IsVisible;

        }
    }
}

