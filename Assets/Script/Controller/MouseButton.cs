﻿namespace Assets.Script.Controller
{
    public enum MouseButton
    {
        LeftButton,
        RightButton,
        CenterButton,
    }

}
