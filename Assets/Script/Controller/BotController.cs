﻿
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script.Controller
{
    public class BotController : MonoBehaviour
    {
        private List<GameObject> _botList = new List<GameObject>();
        private List<GameObject> _botSpawnList = new List<GameObject>();

        private void Start()
        {
            GameObject[] tempBotSpawn = GameObject.FindGameObjectsWithTag("BotSpawn");
            foreach (var spawn in tempBotSpawn)
            {
                _botSpawnList.Add(spawn);
            }
        }

        private void Update()
        {
            if (_botList.Count < 2)
            {
                foreach (GameObject spawn in _botSpawnList)
                {
                    GameObject bot = ObjectPooler.Instance.GetPooledGameObject("Bot") ;
                    bot.transform.position = spawn.transform.position;
                    bot.SetActive(true);
                    AddBotToList(bot);
                    
                }
            }
        }

        public List<GameObject> GetBotList
        {
            get { return _botList; }
            private set { _botList = value; }
        }
        public void AddBotToList(GameObject bot)
        {
            if (!GetBotList.Contains(bot))
            {
                GetBotList.Add(bot);
            }
        }
        public void RemoveBotToList(GameObject bot)
        {
            if (GetBotList.Contains(bot))
            {
                GetBotList.Remove(bot);
            }
        }
    }
}
