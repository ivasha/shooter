﻿using System;
using UnityEngine;

namespace Assets.Script.Controller
{
    class InputController : BaseController
    {
        private int ScrollValue = 0;
        private int _countWeapon = 0;

        private void Update()
        {

            if (Input.GetKeyDown(KeyCode.F))
            {
                Main.Instance.FlasLightController.Switch();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Main.Instance.Inventory.ActivateWeapon(0);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Main.Instance.Inventory.DeactivateWeapon(ScrollValue);
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                Main.Instance.Inventory.Remove();
            }

            _countWeapon = Main.Instance.Inventory.CountWeapon;


            GetScrollValue();
            if (Main.Instance.Inventory.IsActiveWeapon)
            {
                SelectWeapon(ScrollValue);
            }
        }

        private void GetScrollValue()
        {
            if (_countWeapon > 0)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    ScrollValue += 1;
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0)
                {
                    ScrollValue -= 1;
                }

                if (ScrollValue < 0)
                {
                    ScrollValue = 0;
                }

                if (ScrollValue >= _countWeapon)
                {
                    ScrollValue = _countWeapon - 1;
                }
            }
        }

        private void SelectWeapon(int index)
        {
            Weapon weapon = Main.Instance.Inventory.GetWeaponByIndex(index);

            if (weapon)
            {
                Main.Instance.Inventory.ActivateWeapon(index);
                Main.Instance.WeaponController.On(weapon);
            }
        }


    }
}
