﻿using UnityEngine;

namespace Assets.Script.Controller
{
    sealed class Main : MonoBehaviour
    {
        [HideInInspector]
        public static  Main Instance { get; private set; }
        [HideInInspector]
        public FlashLightController FlasLightController { get; private set; }
        [HideInInspector]
        public Inventory Inventory;
        [HideInInspector]
        public WeaponController WeaponController;

        private GameObject _allControllers;
        private InputController _inputController;
        private SelectionController _selectionController;

        private void Awake()
        {
            Instance = this;
            _allControllers = new GameObject("All Controles");
            _inputController = _allControllers.AddComponent<InputController>();
            _selectionController = _allControllers.AddComponent<SelectionController>();
            Inventory = _allControllers.AddComponent<Inventory>();
            FlasLightController = _allControllers.AddComponent<FlashLightController>();
            WeaponController = _allControllers.AddComponent<WeaponController>();

            DontDestroyOnLoad(gameObject);
        }
    }
}
