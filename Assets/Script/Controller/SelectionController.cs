﻿using Assets.Script.Interface;
using UnityEngine;

namespace Assets.Script.Controller
{
    public class SelectionController : MonoBehaviour
    {
        private Camera _mainCamera;
        private Vector2 _center;
        private float _didecateDistance = 20;
        private UIMessage _message;
        private ISelectObj _selectObj;
        private Inventory _inventory;

        private void Awake()
        {
            _mainCamera = Camera.main;
            _center = new Vector2(Screen.width / 2, Screen.height / 2);
            _message = FindObjectOfType<UIMessage>();
        }

        void Start()
        {
            _inventory = FindObjectOfType<Inventory>();
        }


        // Update is called once per frame
        private void Update()
        {
            Ray ray = _mainCamera.ScreenPointToRay(_center);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, _didecateDistance))
            {
                SelectObject(hit.collider.gameObject);
                if (_selectObj != null)
                {
                    if (Input.GetKeyDown(_selectObj.GetKeyCode()))
                    {
                        _inventory.AddObjectToInventory(_selectObj);
                    }
                }
            }

        }

        private void SelectObject(GameObject _gameObject)
        {
            if (!_gameObject is ISelectObj) return;
            _selectObj = _gameObject.GetComponent<ISelectObj>();
            if (_selectObj != null)
            {
                if (_message)
                    _message.SetText(_selectObj.GetMessage());

            }
            else
            {
                if (_message)
                    _message.SetText(string.Empty);
            }
        }
    }
}

