﻿using UnityEngine;

namespace Assets.Script.Controller
{
   public abstract class BaseController : MonoBehaviour
    {
        protected bool _enabled;

        protected bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public virtual void On()
        {
            _enabled = true;
        }

        public virtual void Off()
        {
            _enabled = false;
        }


    }
}
