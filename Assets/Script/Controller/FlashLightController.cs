﻿namespace Assets.Script.Controller
{
    class FlashLightController : BaseController
    {
        private FlashLight _flashLight;
        private UIBatteryCharge _uiBatteryCharge;

        private void Start ()
        {
            _flashLight = FindObjectOfType<FlashLight>();
            _uiBatteryCharge = FindObjectOfType<UIBatteryCharge>();
        }

        private void Update()
        {
            if (!Enabled)
            {
                ReestablishWorkingHours();
                return;
            }

            ReduceWorkingHours();
        }

        
        private void ReduceWorkingHours()
        {
            if (_flashLight.CurrentWorkingHours > 0 && _flashLight.enabled)
            {
                _flashLight.CurrentWorkingHours--;
                _uiBatteryCharge.SetColorBattery(_flashLight.CurrentWorkingHours, true);
            } else
            {
                _uiBatteryCharge.SetColorBattery(0, false);
                Off();
            }
        }

        private void ReestablishWorkingHours()
        {
            if (_flashLight.CurrentWorkingHours < _flashLight.MaxWorkingHours)
            {
                _flashLight.CurrentWorkingHours++;
            }
        }

        public override void On()
        {
            if (Enabled) return;
            base.On();
            Enabled = true;
            SetData(true);
        }

        public override void Off()
        {
            if (!Enabled) return;
            base.Off();
            Enabled = false;
            SetData(false);
            _uiBatteryCharge.SetColorBattery(0, false);


        }

        private void SetData (bool value)
        {
            _flashLight.Switch(value);

        }

        public void Switch()
        {
            if (Enabled)
            {
                Off();
            }
            else
            {
                On();
            }
        }
    }
}
