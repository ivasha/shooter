﻿using Assets.Script.Interface;
using UnityEngine;

namespace Assets.Script
{
    public abstract class Character : BaseObjectScene, ISetDamage
    {
        [SerializeField] protected float _health = 100;
        protected bool _isDead = false;

        public virtual void ApplyDamage(float damage)
        {
            if (_health > 0)
            {
                _health -= damage;
            }

            if (_health <= 0)
            {
                DeadCharacter();
            }
        }

        protected virtual void DeadCharacter()
        {
            _health = 0;
            Color = Color.red;
            _isDead = true;
        }
    }
}
