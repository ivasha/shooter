﻿using Assets.Script;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MyScript))]
public class MyScriptЕditor : Editor
{
    public override void OnInspectorGUI() // Стандартная функция прорисовки
    {
        DrawDefaultInspector(); // Сначала рисуем стандартный функционал
        MyScript ms = target as MyScript; // Ссылка на скрип из которого будем вызывать функции
        if (GUILayout.Button("Создать объекты", EditorStyles.miniButtonMid))
        {
            if (ms.ObjectInstance)
            {
                ms.CreateObj();
            }
        }
        EditorGUILayout.HelpBox("Создание объектов по кнопке", MessageType.Info);
    }
}
